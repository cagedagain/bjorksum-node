const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./bjorksum.db', sqlite3.OPEN_READONLY, err => {
	if (err) {
		console.error(err.message);
	}
	console.log('Connected to the bjorksum database.');
});
exports.db = db;
