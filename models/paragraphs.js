let { db } = require('../db');
const { chunk, reduce } = require('lodash');

const rowMultiplier = 5;

const formatParagraphResponse = (rows, showTitle) => {
	const paragraphs = chunk(rows, rowMultiplier).map(paragraphRows => {
		return paragraphRows
			.reduce((acc, row) => {
				return acc + ' ' + row.paragraph;
			}, '')
			.trim();
	});
	const paragraphsHTML = paragraphs.reduce((acc, paragraph) => {
		return acc + '<p>' + paragraph + '</p>';
	}, '');

	if (showTitle === true) {
		const title = rows[0].title;
		return {
			title: title,
			paragraphs: paragraphs,
			paragraphsHTML: '<h1>' + title + '</h1>' + paragraphsHTML
		};
	} else {
		return {
			paragraphs: paragraphs,
			html: paragraphsHTML
		};
	}
};
const getParagraphs = (limit = 10, showTitle = false, cb) => {
	db.all(`SELECT * FROM paragraphs ORDER BY RANDOM() LIMIT ?`, [limit * rowMultiplier], (err, rows) => {
		cb(err, formatParagraphResponse(rows, showTitle));
	});
};
exports.getParagraphs = getParagraphs;
