bjorksum
========

A lorum ipsum generator using Bjorks lyrics.

What is bjorksum?
-----------------
bjorksum is a simple lorum ipsum generator consisting of Bjork lyrics. This is the node version of a vPHP version I built around 8 years ago.

How to use bjorksum
-------------------
Start:

`npm run start`

Endpoint:

`http://localhost:4001/`

Query Params:

- `limit`: number of paragraphs returned (default: 10)
- `showTitle`: display the song title as a header to the paragraph(s)
