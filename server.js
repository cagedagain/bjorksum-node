const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const paragraphsModel = require('./models/paragraphs');

// Setup body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//CORS settings
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	next();
});

const router = express.Router();

router.get('/', (req, res) => {
	const { limit, showTitle } = req.query;
	paragraphsModel.getParagraphs(limit, showTitle === 'true', (err, paragraphs) => {
		if (err) {
			console.error(err);
		}
		res.json(paragraphs);
	});
});

app.use('/', router);

app.listen(4001);
console.log('Server Started on port ' + 4001);
